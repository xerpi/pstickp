#include <pspkernel.h>
#include <pspdebug.h>
#include <pspdisplay.h>
#include <pspctrl.h>
#include "usb.h"
#include <string.h>
#include "utils.h"

#define PSTICKP_DRIVERNAME "PStickPUSBdriver"
#define PSTICKP_PRODUCT_ID (0x0727)

PSP_MODULE_INFO("PStickP", PSP_MODULE_KERNEL, 1, 1);
PSP_MAIN_THREAD_ATTR(0);

#define ENABLE_DEBUG 0

#if ENABLE_DEBUG == 1
#define DEBUG(...) printf(__VA_ARGS__)
#else
#define DEBUG(...)  (void)0
#endif

#define HID_DESCRIPTOR_HID    0x21
#define HID_DESCRIPTOR_REPORT 0x22

#define HID_REQUEST_GET_REPORT 0x01
#define HID_REQUEST_SET_IDLE   0x0A

#define EVF_CONNECTED		(1 << 0)
#define EVF_DISCONNECTED	(1 << 1)
#define EVF_EXIT		(1 << 2)
#define EVF_INT_REQ_COMPLETED	(1 << 3)
#define EVF_ALL_MASK		(EVF_INT_REQ_COMPLETED | (EVF_INT_REQ_COMPLETED - 1))

struct gamepad_report_t {
	uint8_t report_id;
	uint16_t buttons;
	int8_t left_x;
	int8_t left_y;
	int8_t right_x;
	int8_t right_y;
} __attribute__((packed));

static int usb_process_request(int arg1, int arg2, struct DeviceRequest *req);
static int usb_change(int interfaceNumber, int alternateSetting);
static int usb_attach(int usb_version);
static void usb_detach(void);
static void usb_configure(int usb_version, int desc_count, struct InterfaceSettings *settings);
static int usb_start(int size, void *args);
static int usb_stop(int size, void *args);
static int send_hid_report_desc(void);
static int send_hid_report(uint8_t report_id);
static int send_string_descriptor(int index);
static int send_hid_report_init(uint8_t report_id);

static SceUID usb_event_flag_id;

static
unsigned char hid_report_descriptor[] __attribute__ ((aligned(64))) = {
	0x05, 0x01,		// Usage Page (Generic Desktop Ctrls)
	0x09, 0x05,		// Usage (Game Pad)
	0xA1, 0x01,		// Collection (Application)
	0xA1, 0x00,		//   Collection (Physical)
	0x85, 0x01,		//     Report ID (1)
	0x05, 0x09,		//     Usage Page (Button)
	0x19, 0x01,		//     Usage Minimum (0x01)
	0x29, 0x10,		//     Usage Maximum (0x10)
	0x15, 0x00,		//     Logical Minimum (0)
	0x25, 0x01,		//     Logical Maximum (1)
	0x95, 0x10,		//     Report Count (16)
	0x75, 0x01,		//     Report Size (1)
	0x81, 0x02,		//     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
	0x05, 0x01,		//     Usage Page (Generic Desktop Ctrls)
	0x09, 0x30,		//     Usage (X)
	0x09, 0x31,		//     Usage (Y)
	0x09, 0x32,		//     Usage (Z)
	0x09, 0x33,		//     Usage (Rx)
	0x15, 0x81,		//     Logical Minimum (-127)
	0x25, 0x7F,		//     Logical Maximum (127)
	0x75, 0x08,		//     Report Size (8)
	0x95, 0x04,		//     Report Count (4)
	0x81, 0x02,		//     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
	0xC0,			//   End Collection
	0xC0,			// End Collection
};

/* HID descriptor */
static
unsigned char hiddesc[] = {
	0x09,					/* bLength */
	HID_DESCRIPTOR_HID,			/* bDescriptorType */
	0x11, 0x01,				/* bcdHID */
	0x00,					/* bCountryCode */
	0x01,					/* bNumDescriptors */
	HID_DESCRIPTOR_REPORT,			/* bDescriptorType */
	sizeof(hid_report_descriptor), 0x00	/* wDescriptorLength */
};

static struct StringDescriptor string_descriptors[2] =
{
	{
		.bLength		 = 16,
		.bDescriptorType = USB_DT_STRING,
		.bString		 = { 'P', 'S', 't', 'i', 'c',  'p', 'P' }
	},
	{
		0,
		USB_DT_STRING
	}
};

/* High speed */

static struct DeviceDescriptor device_descriptor_hi =
{
	.bLength		= USB_DT_DEVICE_SIZE,
	.bDescriptorType	= USB_DT_DEVICE,
	.bcdUSB			= 0x0200,
	.bDeviceClass		= USB_CLASS_PER_INTERFACE,
	.bDeviceSubClass	= 0x00,
	.bDeviceProtocol	= 0x00,
	.bMaxPacketSize0	= 0x40,
	.idVendor		= 0x054C, //u wot m8?
	.idProduct		= PSTICKP_PRODUCT_ID,
	.bcdDevice		= 0x0100,
	.iManufacturer	  	= 0x00,
	.iProduct		= 0x00,
	.iSerialNumber	  	= 0x00,
	.bNumConfigurations	= 0x01
};

struct EndpointDescriptor endpoint_descriptors_hi[2] =
{
	{
		.bLength	  = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType  = USB_DT_ENDPOINT,
		.bEndpointAddress = 0x81, //IN, EP address 1
		.bmAttributes	  = USB_ENDPOINT_TYPE_INTERRUPT,
		.wMaxPacketSize   = 0x40,
		.bInterval	  = 0x01
	},
	{
		0
	}
};

struct InterfaceDescriptor interface_descriptors_hi[2] =
{
	{
		.bLength		= USB_DT_INTERFACE_SIZE,
		.bDescriptorType	= USB_DT_INTERFACE,
		.bInterfaceNumber   	= 0x00,
		.bAlternateSetting  	= 0x00,
		.bNumEndpoints	  	= 0x01,
		.bInterfaceClass	= USB_CLASS_HID, //3 = HID
		.bInterfaceSubClass	= 0x00,
		.bInterfaceProtocol 	= 0x00,
		.iInterface		= 0x01,
		.endpoints		= &endpoint_descriptors_hi[0],
		.extra			= hiddesc,
		.extraLength		= sizeof(hiddesc)
	},
	{
		0
	}
};

static struct InterfaceSettings interface_settings_hi[1] =
{
	{
		.descriptors	  = &interface_descriptors_hi[0],
		.alternateSetting = 0,
		.numDescriptors   = 1
	}
};

struct ConfigDescriptor config_descriptor_hi =
{
	.bLength		 = USB_DT_CONFIG_SIZE,
	.bDescriptorType	 = USB_DT_CONFIG,
	.wTotalLength		 = (USB_DT_INTERFACE_SIZE + USB_DT_CONFIG_SIZE + USB_DT_ENDPOINT_SIZE + sizeof(hiddesc)),
	.bNumInterfaces	 	 = 0x01,
	.bConfigurationValue 	 = 0x01,
	.iConfiguration	  	 = 0x00,
	.bmAttributes		 = 0xC0,
	.bMaxPower		 = 0x00,
	.settings		 = &interface_settings_hi[0]
};

static struct UsbConfiguration usb_configuration_hi =
{
	.configDescriptors	= &config_descriptor_hi,
	.settings		= &interface_settings_hi[0],
	.interfaceDescriptors	= &interface_descriptors_hi[0],
	.endpointDescriptors	= &endpoint_descriptors_hi[0]
};

/* Full speed */

static struct DeviceDescriptor device_descriptor_full =
{
	.bLength		= USB_DT_DEVICE_SIZE,
	.bDescriptorType	= USB_DT_DEVICE,
	.bcdUSB			= 0x0110,
	.bDeviceClass	 	= USB_CLASS_PER_INTERFACE,
	.bDeviceSubClass	= 0x00,
	.bDeviceProtocol	= 0x00,
	.bMaxPacketSize0	= 0x08,
	.idVendor		= 0x054C, //u wot m8?
	.idProduct		= PSTICKP_PRODUCT_ID,
	.bcdDevice		= 0x0200,
	.iManufacturer	  	= 0x00,
	.iProduct		= 0x00,
	.iSerialNumber	 	= 0x00,
	.bNumConfigurations 	= 0x01
};

struct EndpointDescriptor endpoint_descriptors_full[2] =
{
	{
		.bLength	  = USB_DT_ENDPOINT_SIZE,
		.bDescriptorType  = USB_DT_ENDPOINT,
		.bEndpointAddress = 0x81, //IN, EP address 1
		.bmAttributes	  = USB_ENDPOINT_TYPE_INTERRUPT,
		.wMaxPacketSize   = 0x40,
		.bInterval	  = 0x0A
	},
	{
		0
	}
};

struct InterfaceDescriptor interface_descriptor_full[2] =
{
	{
		.bLength		= USB_DT_INTERFACE_SIZE,
		.bDescriptorType	= USB_DT_INTERFACE,
		.bInterfaceNumber	= 0x00,
		.bAlternateSetting  	= 0x00,
		.bNumEndpoints	  	= 0x01,
		.bInterfaceClass	= USB_CLASS_HID, //3 = HID
		.bInterfaceSubClass 	= 0x00,
		.bInterfaceProtocol 	= 0x00,
		.iInterface		= 0x00,
		.endpoints		= &endpoint_descriptors_full[0],
		.extra			= hiddesc,
		.extraLength		= sizeof(hiddesc)
	},
	{
		0
	}
};

static struct InterfaceSettings interface_settings_full[1] =
{
	{
		.descriptors	  = &interface_descriptor_full[0],
		.alternateSetting = 0,
		.numDescriptors   = 1
	}
};

struct ConfigDescriptor config_descriptor_full =
{
	.bLength		= USB_DT_CONFIG_SIZE,
	.bDescriptorType	= USB_DT_CONFIG,
	.wTotalLength		= (USB_DT_INTERFACE_SIZE + USB_DT_CONFIG_SIZE + USB_DT_ENDPOINT_SIZE + sizeof(hiddesc)),
	.bNumInterfaces	  	= 0x01,
	.bConfigurationValue	= 0x01,
	.iConfiguration		= 0x00,
	.bmAttributes		= 0xC0,
	.bMaxPower		= 0x00,
	.settings		= &interface_settings_full[0],
};

static struct UsbConfiguration usb_configuration_full =
{
	.configDescriptors	= &config_descriptor_full,
	.settings		= &interface_settings_full[0],
	.interfaceDescriptors 	= &interface_descriptor_full[0],
	.endpointDescriptors  	= &endpoint_descriptors_full[0]
};

static struct UsbEndpoint usb_endpoints[3] =
{
	{0, 0, 0},
	{1, 0, 0},
};

static struct UsbInterface usb_interfaces[1] =
{
	{-1, 0, 1}
};

static struct UsbDriver usb_driver =
{
	PSTICKP_DRIVERNAME,
	2,
	&usb_endpoints[0],
	&usb_interfaces[0],
	&device_descriptor_hi,
	&usb_configuration_hi,
	&device_descriptor_full,
	&usb_configuration_full,
	&string_descriptors[0],
	&usb_process_request,
	&usb_change,
	&usb_attach,
	&usb_detach,
	&usb_configure,
	&usb_start,
	&usb_stop,
	NULL
};

int main(int argc, char *argv[])
{
	pspDebugScreenInit();
	SetupCallbacks();

	printf("PStickP by xerpi\n");

	printf("Registering USB driver...");
	int ret = sceUsbbdRegister(&usb_driver);
	printf("returned %i\n", ret);

	printf("Starting PSP_USBBUS...");
	ret = sceUsbStart(PSP_USB_BUS_DRIVERNAME, 0, 0);
	printf("returned %i\n", ret);

	printf("Starting "PSTICKP_DRIVERNAME"...");
	ret = sceUsbStart(PSTICKP_DRIVERNAME, 0, 0);
	printf("returned 0x%08X\n", ret);

	printf("Activating 0x%04X...", PSTICKP_PRODUCT_ID);
	ret = sceUsbActivate(PSTICKP_PRODUCT_ID);
	printf("returned 0x%08X\n", ret);

	usb_event_flag_id = sceKernelCreateEventFlag("usb_evflag", 0, 0, NULL);

	sceCtrlSetSamplingCycle(0);
	sceCtrlSetSamplingMode(1);

	int connected = 0;

	while (run) {
		int ret;
		unsigned int evf_out;

		ret = sceKernelWaitEventFlagCB(usb_event_flag_id, EVF_ALL_MASK,
			PSP_EVENT_WAITOR | PSP_EVENT_WAITCLEAR, &evf_out, NULL);
		if (ret < 0)
			break;

		DEBUG("Event flag: 0x%08X\n", evf_out);

		if (evf_out & EVF_EXIT) {
			break;
		} else if (evf_out & EVF_CONNECTED) {
			if (send_hid_report(1) >= 0) {
				printf("Connected!\n");
				connected = 1;
			}
		} else if (evf_out & EVF_DISCONNECTED) {
			printf("Disconnected!\n");
			connected = 0;
		} else if (evf_out & EVF_INT_REQ_COMPLETED && connected) {
			send_hid_report(1);
		}
	}

	printf("Exiting!\n");

	sceKernelSetEventFlag(usb_event_flag_id, EVF_EXIT);
	sceKernelDelayThread(10000);
	sceKernelDeleteEventFlag(usb_event_flag_id);


	sceUsbDeactivate(); //PSTICKP_PRODUCT_ID??
	sceUsbStop(PSTICKP_DRIVERNAME, 0, 0 );
	sceUsbStop(PSP_USB_BUS_DRIVERNAME, 0, 0);
	sceUsbbdUnregister(&usb_driver);
	sceKernelExitGame();
	return 0;
}

static int send_hid_report_desc(void)
{
	static struct UsbbdDeviceRequest req = {
		.endpoint = &usb_endpoints[0],
		.data = hid_report_descriptor,
		.size = sizeof(hid_report_descriptor),
		.isControlRequest = 0,
		.onComplete = NULL,
		.transmitted = 0,
		.returnCode = 0,
		.next = NULL,
		.unused = NULL,
		.physicalAddress = NULL
	};

	return sceUsbbdReqSend(&req);
}

static int send_string_descriptor(int index)
{
	static struct UsbbdDeviceRequest req = {
		.endpoint = &usb_endpoints[0],
		.data = &string_descriptors[0],
		.size = sizeof(string_descriptors[0]),
		.isControlRequest = 0,
		.onComplete = NULL,
		.transmitted = 0,
		.returnCode = 0,
		.next = NULL,
		.unused = NULL,
		.physicalAddress = NULL
	};

	return sceUsbbdReqSend(&req);
}

static int send_hid_report_init(uint8_t report_id)
{
	static struct gamepad_report_t gamepad __attribute__((aligned(64))) = {
		.report_id = 1,
		.buttons = 0,
		.left_x = 0,
		.left_y = 0,
		.right_x = 0,
		.right_y = 0
	};

	static struct UsbbdDeviceRequest req = {
		.endpoint = &usb_endpoints[0],
		.data = &gamepad,
		.size = sizeof(gamepad),
		.isControlRequest = 0,
		.onComplete = NULL,
		.transmitted = 0,
		.returnCode = 0,
		.next = NULL,
		.unused = NULL,
		.physicalAddress = NULL
	};

	return sceUsbbdReqSend(&req);
}

static void hid_report_on_complete(struct UsbbdDeviceRequest *req)
{
	DEBUG("hid_report_on_complete\n");

	sceKernelSetEventFlag(usb_event_flag_id, EVF_INT_REQ_COMPLETED);
}

static void fill_gamepad_report(const SceCtrlData *pad, struct gamepad_report_t *gamepad)
{
	gamepad->report_id = 1;
	gamepad->buttons = 0;

	if (pad->Buttons & PSP_CTRL_SQUARE)
		gamepad->buttons |= 1 << 0;
	if (pad->Buttons & PSP_CTRL_CROSS)
		gamepad->buttons |= 1 << 1;
	if (pad->Buttons & PSP_CTRL_CIRCLE)
		gamepad->buttons |= 1 << 2;
	if (pad->Buttons & PSP_CTRL_TRIANGLE)
		gamepad->buttons |= 1 << 3;

	if (pad->Buttons & PSP_CTRL_LTRIGGER)
		gamepad->buttons |= 1 << 4;
	if (pad->Buttons & PSP_CTRL_RTRIGGER)
		gamepad->buttons |= 1 << 5;

	//if (pad->Buttons & PSP_CTRL_L1)
	//	gamepad->buttons |= 1 << 6;
	//if (pad->Buttons & PSP_CTRL_R1)
	//	gamepad->buttons |= 1 << 7;

	if (pad->Buttons & PSP_CTRL_SELECT)
		gamepad->buttons |= 1 << 8;
	if (pad->Buttons & PSP_CTRL_START)
		gamepad->buttons |= 1 << 9;

	//if (pad->Buttons & PSP_CTRL_L3)
	//	gamepad->buttons |= 1 << 10;
	//if (pad->Buttons & PSP_CTRL_R3)
	//	gamepad->buttons |= 1 << 11;

	if (pad->Buttons & PSP_CTRL_UP)
		gamepad->buttons |= 1 << 12;
	if (pad->Buttons & PSP_CTRL_DOWN)
		gamepad->buttons |= 1 << 13;
	if (pad->Buttons & PSP_CTRL_RIGHT)
		gamepad->buttons |= 1 << 14;
	if (pad->Buttons & PSP_CTRL_LEFT)
		gamepad->buttons |= 1 << 15;

	gamepad->left_x = (int8_t)pad->Lx - 128;
	gamepad->left_y = (int8_t)pad->Ly - 128;
	gamepad->right_x = 0;
	gamepad->right_y = 0;
}

static int send_hid_report(uint8_t report_id)
{
	static struct gamepad_report_t gamepad __attribute__((aligned(64))) = { 0 };
	SceCtrlData pad;

	sceCtrlPeekBufferPositive(&pad, 1);

	fill_gamepad_report(&pad, &gamepad);

	sceKernelDcacheWritebackRange(&gamepad, sizeof(gamepad));

	static struct UsbbdDeviceRequest req = {
		.endpoint = &usb_endpoints[1],
		.data = &gamepad,
		.size = sizeof(gamepad),
		.isControlRequest = 0,
		.onComplete = hid_report_on_complete,
		.transmitted = 0,
		.returnCode = 0,
		.next = NULL,
		.unused = NULL,
		.physicalAddress = NULL
	};

	return sceUsbbdReqSend(&req);
}

int usb_process_request(int recipient, int arg, struct DeviceRequest *req)
{
	DEBUG("usb_driver_process_request\n");

	DEBUG("recvctl: %x %x\n", recipient, arg);
	DEBUG("request: %x type: %x wValue: %x wIndex: %x wLength: %x\n",
		req->bRequest, req->bmRequestType, req->wValue, req->wIndex, req->wLength);

	if (arg < 0)
		return -1;

	uint8_t req_dir = req->bmRequestType & USB_REQ_TYPE_DIRECTION;
	uint8_t req_type = req->bmRequestType & USB_REQ_TYPE_TYPE;
	uint8_t req_recipient = req->bmRequestType & USB_REQ_TYPE_RECIPIENT;

	if (req_dir == USB_REQ_TYPE_IN) {
		DEBUG("Device to host\n");

		switch (req_type) {
		case USB_REQ_TYPE_STANDARD:
			switch (req_recipient) {
			case USB_REQ_TYPE_DEVICE:
				switch (req->bRequest) {
				case USB_REQ_GET_DESCRIPTOR: {
					uint8_t descriptor_type = (req->wValue >> 8) & 0xFF;
					uint8_t descriptor_idx = req->wValue & 0xFF;

					switch (descriptor_type) {
					case USB_DT_STRING:
						send_string_descriptor(descriptor_idx);
						break;
					}
					break;
				}

				}
				break;
			case USB_REQ_TYPE_INTERFACE:
				switch (req->bRequest) {
				case USB_REQ_GET_DESCRIPTOR: {
					uint8_t descriptor_type = (req->wValue >> 8) & 0xFF;
					uint8_t descriptor_idx = req->wValue & 0xFF;

					switch (descriptor_type) {
					case HID_DESCRIPTOR_REPORT:
						send_hid_report_desc();
						break;
					}
				}

				}
				break;
			}
			break;
		case USB_REQ_TYPE_CLASS:
			switch (recipient) {
			case USB_REQ_TYPE_INTERFACE:
				switch (req->bRequest) {
				case HID_REQUEST_GET_REPORT: {
					uint8_t report_type = (req->wValue >> 8) & 0xFF;
					uint8_t report_id = req->wValue & 0xFF;

					if (report_type == 1)/* Input report type */
						send_hid_report_init(report_id);
					break;
				}

				}
				break;
			}
			break;
		}
	} else if (req_dir == USB_REQ_TYPE_OUT) {
		DEBUG("Host to device\n");

		switch (req_type) {
		case USB_REQ_TYPE_CLASS:
			switch (req_recipient) {
			case USB_REQ_TYPE_INTERFACE:
				switch (req->bRequest) {
				case HID_REQUEST_SET_IDLE:
					DEBUG("Set idle!\n");
					break;
				}
				break;
			}
			break;
		}
	}

	return 0;
}

int usb_change(int interfaceNumber, int alternateSetting)
{
	DEBUG(PSTICKP_DRIVERNAME" Interface Changed\n");
	return 0;
}

int usb_attach(int usb_version)
{
	DEBUG(PSTICKP_DRIVERNAME" attached.\n");

	sceUsbbdClearFIFO(&usb_endpoints[1]);
	sceUsbbdReqCancelAll(&usb_endpoints[1]);

	sceKernelSetEventFlag(usb_event_flag_id, EVF_CONNECTED);

	return 0;
}

void usb_detach(void)
{
	DEBUG(PSTICKP_DRIVERNAME" detached.\n");
	sceKernelSetEventFlag(usb_event_flag_id, EVF_DISCONNECTED);
}

void usb_configure(int usb_version, int desc_count, struct InterfaceSettings *settings)
{
	DEBUG(PSTICKP_DRIVERNAME" configure.\n");
}

int usb_start(int size, void *args)
{
	DEBUG(PSTICKP_DRIVERNAME" started.\n");
	return 0;
}

int usb_stop(int size, void *args)
{
	DEBUG(PSTICKP_DRIVERNAME" stopped.\n");
	return 0;
}


